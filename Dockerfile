FROM node:16.14-alpine3.15 as build

WORKDIR /app/website

COPY ./website /app/website
RUN yarn install && yarn build
#############################################
FROM nginx:1.23.2-alpine

WORKDIR /var/www
COPY --from=build /app/website/build/ /usr/share/nginx/html/
