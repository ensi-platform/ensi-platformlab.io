import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './styles.module.css';

const domains = [
    {
        title: 'Customer GUI',
        services: [
            {
                title: 'Customer api gateway',
                link: 'https://customer-api-gateway-master-dev.ensi.tech/docs/oas#/',
            },
        ],
    },
    //{
    //    title: 'Reviews',
    //    services: [
    //        {
    //            title: 'Reviews',
    //            link: 'https://reviews-master-dev.ensi.tech/docs/oas#/',
    //        },
    //    ],
    //},
    //{
    //    title: 'Seller GUI',
    //    services: [
    //        {
    //            title: 'Seller GUI Backend',
    //            link: 'https://seller-gui-backend-master-dev.ensi.tech/docs/oas#/',
    //        },
    //    ],
    //},
    {
        title: 'Admin GUI',
        services: [
            {
                title: 'Admin GUI Backend',
                link: 'https://admin-gui-backend-master-dev.ensi.tech/docs/oas#/',
            },
        ],
    },
    {
        title: 'Communication',
        services: [
            {
                title: 'Internal messenger',
                link: 'https://internal-messenger-master-dev.ensi.tech/docs/oas#/',
            },
            {
                title: 'Communication manager',
                link: 'https://communication-master-dev.ensi.tech/docs/oas#/',
            },
        ],
    },
    {
        title: 'Units',
        services: [
            {
                title: 'Admin Auth',
                link: 'https://admin-auth-master-dev.ensi.tech/docs/oas#/',
            },
            {
                title: 'Business Units',
                link: 'https://bu-master-dev.ensi.tech/docs/oas#/',
            },
            //{
            //    title: 'Seller Auth',
            //    link: 'https://seller-auth-master-dev.ensi.tech/docs/oas#/',
            //},
        ],
    },
    {
        title: 'Marketing',
        services: [
            {
                title: 'Marketing',
                link: 'https://marketing-master-dev.ensi.tech/docs/oas#/',
            },
        ],
    },
    {
        title: 'Catalog',
        services: [
            {
                title: 'Catalog Cache',
                link: 'https://catalog-cache-master-dev.ensi.tech/docs/oas#/',
            },
            {
                title: 'PIM',
                link: 'https://pim-master-dev.ensi.tech/docs/oas#/',
            },
            {
                title: 'Offers',
                link: 'https://offers-master-dev.ensi.tech/docs/oas#/',
            },
            //{
            //    title: 'Feed',
            //    link: 'https://feed-master-dev.ensi.tech/docs/oas#/',
            //},
        ],
    },
    {
        title: 'Orders',
        services: [
            {
                title: 'OMS',
                link: 'https://oms-master-dev.ensi.tech/docs/oas#/',
            },
            {
                title: 'Baskets',
                link: 'https://baskets-master-dev.ensi.tech/docs/oas#/',
            },
            //{
            //    title: "Packing",
            //    link: "https://packing-master-dev.ensi.tech/docs/oas#/"
            //},
        ],
    },
    {
        title: 'Logistic',
        services: [
            {
                title: 'Logistic',
                link: 'https://logistic-master-dev.ensi.tech/docs/oas#/',
            },
            //{
            //    title: "Geo",
            //    link: "https://geo-master-dev.ensi.tech/docs/oas#/"
            //},
        ],
    },
    {
        title: 'CMS',
        services: [
            {
                title: 'CMS',
                link: 'https://cms-master-dev.ensi.tech/docs/oas#/',
            },
        ],
    },
    {
        title: 'Customers',
        services: [
            {
                title: 'CRM',
                link: 'https://crm-master-dev.ensi.tech/docs/oas#/',
            },
            {
                title: 'Customer Auth',
                link: 'https://customer-auth-master-dev.ensi.tech/docs/oas#/',
            },
            {
                title: 'Customers',
                link: 'https://customers-master-dev.ensi.tech/docs/oas#/',
            },
        ],
    },
];

function Domain({title, services}) {
    return (
        <div className={clsx('col col--4 margin-top--md', styles.feature)}>
            <h4>{title}</h4>
            {services.map((apiInfo, idx) => (
                <Api key={idx} {...apiInfo} />
            ))}
        </div>
    );
}

function Api({title, link}) {
    return (
        <li><a href={link} target="_blank">{title}</a></li>
    );
}

export default function Home() {
    const context = useDocusaurusContext();
    return (
        <Layout
            title="OAS" description="Description will go into a meta tag in <head />"
        >
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">Api</h1>
        </div>
      </header>
      <main>
        {domains && domains.length > 0 && (
            <section className={styles.features}>
            <div className="container">
              <div className="row">
                {domains.map((domain, idx) => (
                    <Domain key={idx} {...domain} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
    );
}
